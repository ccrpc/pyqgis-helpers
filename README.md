# PyQGIS Cross-Platform Helpers
Helpers to create cross-platform scripts using PyQGIS

## Installation

### Mac and Linux
```
sudo curl -L https://gitlab.com/ccrpc/pyqgis-helpers/raw/master/qgis-python -o /usr/local/bin/qgis-python
sudo chmod +x /usr/local/bin/qgis-python
```

### Windows
```
mklink C:\OSGeo4W64\bin\qgis-python.bat C:\OSGeo4W64\bin\python-qgis.bat
setx QGIS_PREFIX_PATH "C:\OSGeo4W64\apps\qgis"
```

## Usage
* Copy `standalone.py`, and edit it.
* Run using:
  ```
  qgis-python standalone.py
  ```

## License
PyQGIS Cross-Platform Helpers is available under the terms of the [BSD 3-clause
license](https://gitlab.com/ccrpc/pyqgis-helpers/blob/master/LICENSE.md).
