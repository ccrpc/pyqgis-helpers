from qgis.core import QgsApplication


LAUNCH_GUI = False
qgs = QgsApplication([], LAUNCH_GUI)
qgs.initQgis()

# Do stuff

qgs.exitQgis()
